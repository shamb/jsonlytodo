# Vanilla JavaScript To-do list

# Contents

1. [Introduction](#markdown-header-introduction).

1. [Files](#markdown-header-files).

1. [Structure and user requirements](#markdown-header-structure-and-user-requirements).

1. [HTML and CSS](#markdown-header-html-and-css).

1. [Data](#markdown-header-data).

1. [JavaScript](#markdown-header-javascript).

1. [FAQ](#markdown-header-faq).

## Introduction

This project was started during a frontend job tech-test; _Create a to-do list using only HTML/CSS and JavaScript._ The expected answer was probably something like [the quick solution at w3schools](https://www.w3schools.com/howto/howto_js_todolist.asp).

That example is a cool tutorial for web design, but doesn't include many expectations seen in web application development, so I thought I'd do something that does, including:

* A modular render path. The view is redrawn based on tasks.
* Efficient rendering. The code only renders parts of the lists that have actually changed.
* Local data persistence to make the application usable day-to-day rather than another code example.
* Responsive design and CSS transitions/keyframe animation.

I now use the to-do list as my day-to-day list of things to do on my dev laptop (just create a link to the ***index.html*** file on your desktop and it will work).

> This repo should make for a good test for your JS abilities - just delete the the ***main.js*** file, and see if you can recreate the functionality of a to-do list (requirements are shown in an Appendix at the end of this document), using the ***index.html*** and ***styles.css*** files as your starting point.

[Click this link to use the completed to-do list](https://shamb.bitbucket.io/example/todo/).

Click on the _New to-do_ text to create a new to-do ticket (as suggested by an attractor message). Enter some text. The text can be multi-line (a scrollbar will appear on the ticket after a few lines if your text is long).

![Creating a new ticket](/readme-resources/contents-02.png)

You can now do a number of things with this ticket, but for now, click the right arrow to mark it as done.

![Marking a ticket as done](/readme-resources/contents-03.png)

Finally, close the ticket by clicking the 'x' icon in the 'Done' column.

Other features of the to-do list are described in [Structure and user requirements](#structure-and-user-requirements)

You can also download the repo and double-click on the ***index.html*** file in the root directory to run it.

[_back to top_](#markdown-header-contents)

## Files

| Filename | Description |
|---|---|
| ***index.html*** | The main html file. Imports ***main.js***, ***styles.css*** and the icon svg files in the `resources` folder. |
| ***styles.css*** | The style file, using standard CSS. |
| ***main.js*** | The JavaScript file, written as an IIFE. |
| resources | The runtime resources (svg icons) used by the application source: [font-awesome](https://fontawesome.com). |
| README md | The file you are reading now. |
| readme-resources | The image files used in the file you are reading now. |

The font-awesome icons used are shown below. The angle-down and angle-up icons were changed from black to white via the svg path _fill_ attribute.

![svg icons](/readme-resources/files-01.png)

[_back to top_](#markdown-header-contents)

## Structure and user requirements

User requirements are listed in the [Appendix](#markdown-header-appendices).

### Application components

There are three components to the application:
The **board** contains two **lists** with CSS class names `todo` and `done`. The lists contain **tickets**.

#### Board

The board encloses the two columns. The board has a maximum width (After which it centers itself). It also maintains 100% height.

![board highlights](/readme-resources/operation-05.png)

#### List

The board contains two lists; 'todo' and 'done'. They are implemented as simple dumb unordered lists (`<ul>`).

#### Tickets

A ticket has a number, message and icons that allow it to be moved around the board.

##### Ticket Icons

Depending on the current context, one or more icons may appear on the ticket. Contextual (i.e. 'hidden unless required) controls allows the ticket to remain uncluttered whilst maintaining all required interactivity.

If the user rolls over the number, the number is overlaid with up/down icons, allowing the ticket to be moved up/down the current list.

One or more icons may appear to the right of the ticket. The left/right arrow icons allow the ticket to be moved between the two lists. The x icon allows the ticket to be deleted.

> Deletion is only allowed on a ticket that is _empty_ or _done_. Both signify a 'spent ticket' so there is no harm in deletion, and therefore no requirement for a confirmation popup.

![ticket icons](/readme-resources/operation-01.png)

##### Ticket message

The ticket has a central message area. It is editable on click (focus) and is saved on click-outside (blur). The message area is not a form input, but a content-editable container. This is to allow the option of rich text formatting later.

The user can edit/re-edit a ticket at any time simply by clicking in the text area in the center of the ticket.

![ticket message](/readme-resources/operation-03.png)

The message text length is not limited - the ticket will **(a)** grow in height to accommodate up to 6 lines, and on the seventh **(b)**, a scrollbar will appear. Outside edit **(c)**, the scrollbar will not appear unless the user rolls over the ticket.

![ticket long message handling](/readme-resources/operation-02.png)

#### Ticket creation

Ticket creation occurs by entering text into the default-new ticket. This ticket always appears at the top of the to-do column. As soon as this ticket has text, it moves down one position to become a new ticket, and a new default-new ticket is created above it. This implementation occurs so there is no need for any icons outside the ticket, i.e. no 'add new' button, '+' button, 'menu > new' option (the idea was taken from a common pattern seen in video game save-screens).

![ticket creation](/readme-resources/operation-04.png)


[_back to top_](#markdown-header-contents)

## HTML and CSS

When the application starts, the screen looks like this:

![initial appearance](/readme-resources/html-01.png)

> The view for mobile devices is discussed in the next section.

The markup for this is static, and  looks like this for desktop:

![static markup](/readme-resources/html-02.png)

The CSS uses flex throughout, except for the centering of the main application (which uses `margin: 0 auto` to set left and right margins to `auto` so that they fill any additional left-right space equally around the board, thus centering it). 

The application's inner rows/columns are created via `display: flex` and `flex-direction: row` and `flex-direction: column`.

To get the application working, our JavaScript needs to add list-item (`li`) elements into the two column `ul` tags. Simple!

We can see how adding tickets will work by doing it manually.

> **Note** - Creating your HTML/CSS and then dry running what your code needs to do by manually moving tags around is a very powerful way to see what your JavaScript will need to do. It also allows you to put as much functionality into your CSS (implementing via CSS as a first call rather than JavaScript, structuring your containers so scrollbar appearing/disappearing occurs without need for code, and ensuring your scrollable areas are optimised for redraw). The last point about scrollable areas being optimised is a big deal in React/Angular/Vue, because the framework does not do this sort of low level optimisation for you. Nothing brings a virtual DOM to its heels faster than un-optimized scroll areas!

The application creates tickets by cloning a hidden section of the HTML. You can see it in the mark-up as the first thing after the `body` tag:

```html
<body>

  <div id="library" class="library" <ul class="list">
    <!--
        library_listItem is cloned in the code to create the list-items.
        The library does not appear in the final application because it
        has display:none set. 
      -->
    <li id="library_listItem" class="listItem">
      <div class="listItemInner">
        <div class="numberContainer">
          <div class="itemNumber">
            x 
          </div>
          <div class="itemMover">
            <div class="moveUp"></div>
            <div class="moveDown"></div>
          </div>
        </div>
        <div class="itemContent" contenteditable="true">xxx xxxx xxxxxx xx</div>
        <div class="iconContainer">
          <div class="moveRight"></div>
          <div class="moveLeft"></div>
          <div class="delete"></div>
        </div>
      </div>
    </li>
    </ul>
  </div>
```

In Chrome devtools, double-click the "library" class text, and change it to "". You will then see the previously hidden `li` 'library_listItem' appear at the top of the screen.

![Showing the library](/readme-resources/html-04.png)

Next, we can see what happens when we manually add one of these `li`s in the `ul`.

Refresh the browser so our previous changes are reverted. Then either select the hidden `li` and press ***Control+C*** or right-click it and select ***Copy > Copy Element***.  

![manually changing the HTML 01](/readme-resources/html-05.png)

Now find the `ul` with id `toDo`. Select it and press ***Control+V***

> **Note** You can find the to-do `li` quickly by using the select icon in the ***Dev Tools > Elements tab***, and click into the empty to-do list).

![manually changing the HTML 02](/readme-resources/html-06.png)

You will see a copy of the `ul` pasted in the HTML.

![manually changing the HTML 03](/readme-resources/html-07.png)

More importantly, can also see the result in the browser;

![manually changing the HTML 04](/readme-resources/html-08.png)

Do the same again, except this time for the done list.

![manually changing the HTML 05](/readme-resources/html-09.png)

Note that the to-to and done `li` renders differently, even though they use the same HTML. Here is the CSS that does this (it's all in ***styles.css***):

![manually changing the HTML 06](/readme-resources/html-10.png)

The todo and done columns have different classes (`todo` and `done`). We can use this fact to make the ticket number circle to be orange/green **(a)**, give the two tickets different highlight colors **(b)** and give the done ticket no move-up/down icon via CSS **(c)**.

> Why don't we also control the appearance of the other ticket icons with CSS? Because this needs to be contextual. The context is only known at runtime through _data_, so we have to use code.

### Responsive design

There are a number of media queries in the CSS.

#### Mobile portrait displays

```css
.container {
  display: flex;
  flex-direction: row;
  max-width: 1000px;
  margin: 0 auto;
  height: 100%;
}
@media only screen and (max-width: 530px) {
  .container {
    flex-direction: column;
  }
}
```

This query overrides the default two column `container` styling into a single-column, 2-row layout for mobile-portrait as shown below.

![Change from 2-column to 1-column layout](/readme-resources/html-11.png)

#### Small width landscape displays

```css
.listItem .iconContainer {
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 2.6em;
}
.listItem .moveLeft,
.listItem .moveRight,
.listItem .delete {
  background-position: center;
  background-repeat: no-repeat;
  background-size: 100%;
  width: 2em;
  height: 2em;
  margin-left: 0.3em;
  opacity: 0.7;
  cursor: pointer;
  transition-property: width, height;
  transition-duration: 0.2s;
  transition-timing-function: ease-in-out;
}
...
...

@media only screen and (min-width:530px) and (max-width: 750px) {
  .listItem .iconContainer {
    flex-direction: column;
    margin-bottom: 0.1em;
  }
  .listItem .moveLeft,
  .listItem .moveRight,
  .listItem .delete {
    width: 1.2em;
    height: 1.2em;
    margin-top: 0.1em;
  }
}
```

Many mobile devices would benefit from a some width optimizations when in landscape, and this is handled by the above media query. The CSS stacks the right hand icons as a column rather than a row, plus makes the icons smaller. Both of these optimizations leave more space for the text area.

![Change from 2-column to 1-column layout](/readme-resources/html-13.png)

This width optimization is also useful for some devices that are marginally wide enough for 2 columns in portrait. Lowering the icon real-estate makes for a less cluttered fit.

![Change from 2-column to 1-column layout](/readme-resources/html-12.png)

#### Font sizing

Finally, under responsive design, we need to consider the user changing text size. All styling that would be expected to size up/down with text is specified in `em`, including;

* Height/width
* Padding/margins
* Border radius
* Animation sizing

### Animation and transitions

The to-do application looks much better with a little animation. This is almost entirely done with CSS animation on the `listItem` class that styles the tickets.

```css
.listItem {
  border: 1px solid #d0d0d0;
  border-radius: 0.3em;
  margin: 0.3em 0.3em 0 0.3em;
  overflow: hidden;
  animation-fill-mode: forwards;
  animation-direction: normal;
}
.listItem.changeFlash {
  animation-name: changeFlash;
  animation-duration: 0.5s;
  animation-timing-function: ease-in-out;
}
.listItem.addItem {
  animation-name: addItem;
  animation-duration: 0.15s;
  animation-timing-function: ease-in-out;
}
.listItem.removeItem {
  animation-name: removeItem;
  animation-duration: 0.15s;
  animation-timing-function: ease-in-out;
}
```

> See ***styles.css*** for the keyframe animations `changeFlash`, `addItem` and `removeItem`.

The `changeFlash` animation flashes any ticket whose text has been changed.

```css
@keyframes changeFlash {
  0% {
    filter: brightness(100%);
  }
  25% {
    filter: brightness(90%);
  }
  100% {
    filter: brightness(100%);
  }
}
```

The addItem and removeItem animations slide in and slide out tickets that are being added and removed respectively.

```css
@keyframes addItem {
  0% {
    max-height: 0em;
  }
  100% {
    max-height: 7.5em;
  }
}
@keyframes removeItem {
  0% {
    max-height: 7.5em;
  }
  100% {
    max-height: 0;
    border-color: transparent;
    border-width: 0;
    margin-top: 0;
  }
}
```

> You may notice that the addItem and removeItem animations do not always start at the exact instant when you move a ticket (which is implemented as a delete from one list and an add to the other). This is because the height animation is taken to be the maximum height of a ticket and not its actual height, and is worst for single line tickets. A way to fix this would be to do it dynamically with JavaScript. I don't think it adds much, as you have to be really looking to detect the problem in any case!


[_back to top_](#markdown-header-contents)

## Data

It is important to drive any sizeable application through _data_. You should not use your view to store data  because you become limited by DOM redraw delays and change race conditions (as used to happen in the days of JQuery!) as well as losing Separation of Concerns between your view and underlying data.

Instead, applications need to be _data driven_. The data for our application looks like this:

### Ticket

| Property | Description |
|---|---|
| `id` | The unique identifier for each ticket. Also used as the _key_ between the data and the DOM element representing it. |
| `isDefaultNew` | `true` signifies this is the default-new ticket (that appears at the top of the to-do column). `false` signifies it is any other ticket. |
| `ticketNumber` | Either `null` or an integer number. This is the number that appears to the left of the ticket |
| `msg` | The Ticket message. |
| `done` | `true` marks the ticket as 'done'. `false` marks it as 'to-do'. This property defines which column the ticket is rendered in. Note this property avoids us having two data ticket lists - we only need one!
| `task` | This property defines what needs to happen per Ticket on data-change. The value can be one of 'none', 'new', 'move', 'delete', 'change' or 'renumber'. This list contains all the operations our code will need to do in response to all possible user interactions. Successfully defining this list actually defined more than half our code! Further, in a larger application, our tasks are scalable - they could easily be refactored into _action_ events.

So what other data do we need?

Nothing!

> All the system needs is a single array of Ticket objects as defined above. We don't need two lists (To-do and Done) because we can just filter the one list on `ticket.done`. We don't need a clever virtual-DOM or other change-buffer either because the `task` property does that for us!

How did I reach this data shape? I started with `ticketNumber`, `msg` and `done` because they relate directly to view so were no-brainers, plus `id` because all data needs one for unique referencing. The other two (`isDefaultNew` and `task`) came about whilst building particular features ('add new ticket' and 'optimize render').

> As an aside, you can see the data and how it changes in the running application by looking at the localStorage object via browser dev-tools. This data updates in real-time on every user interaction.

[_back to top_](#markdown-header-contents)

## JavaScript

### Main code

> **Top level description**: The main code sets up the data, then calls are made in the order `render()` > `renderList()` > `renderTicket()` to render the full view. After initial render, events on the tickets make changes to the underlying data (`listData`) on user interaction, and these data changes trigger redraws via calls to `render()` using the new data.

The main code looks like this;

```javascript
(function main() {
  ...
  ...
  ...

  /**
   * Initialisation.
   * 1. Create the initial data (consisting of one default ticket) and any constants or the last data seen in localStorage.
   * 2. Find the two lists (todo and done).
   * 3. Do the initial render.
   * 4. Add interactivity for the 'attractor' message if necessary, otherwise delete the attractor.
   *
   * The application will then continue to run based on user interaction
   * i.e. adding/moving/deleting tickets.
   *  */

  const NEW_TO_DO = "New to-do";
  const TASK_NONE = "none";
  const TASK_NEW = "actionNew";
  const TASK_MOVE = "actionMove";
  const TASK_DELETE = "actionDelete";
  const TASK_CHANGE = "actionChange";
  const TASK_RENUMBER = "actionRenumber";
  // 1
  let localStorage = window.localStorage;
  let listData = getLocalStorage(localStorage);
  getLocalStorage(localStorage);
  // 2
  const toDoDOM = document.getElementById("toDo");
  const doneDOM = document.getElementById("done");
  // 3
  render(true);
  // 4
  if (listData.length > 1) {
    document.getElementById("attractor").remove();
  } else {
    document.getElementById("container").addEventListener("click", stopAttractor);
  }
})();
```

The entire script is enclosed in an IIFE (immediately invoked function expression). It's an old way of emulating modern modules.

Ignoring all the function definitions, the main code is as shown. We define some constants (`NEW_TO_DO` is the default text in a new ticket, and the remaining constants are our task names introduced in [Data](#markdown-header-data)).

We then initialise our ticket list, `listData`, which will be either the list previously stored in `localStorage` or a new empty list. Then we run `render()`.

Finally, we control the 'attractor' message that pops up if the list is empty on start. This message nudges the user to click on the _New to-do_ text to create their first ticket.

### render()

> **Top level description**: `render()` splits our main list into to-do and done lists, then once all pending render tasks are done, resets the render tasks to 'done`, and finally, saves the just rendered data to localStorage.

The render function looks like this:

```javascript
function render(isFirstRender=false) {
  // Renders the application.
  // Takes the single toDo list data, splitting it into 'todo' and
  // 'done' lists, and then render into the two lists.
  // Note that a move has to be in both lists (because is is a
  // 'delete' in one list and an 'add' in the other).
  const callback = () => {
    const toDoListData = listData.filter(
      (item) => !item.done || item.task === TASK_MOVE
    );
    const doneListData = listData.filter(
      (item) => item.done || item.task === TASK_MOVE
    );
    renderList(toDoListData, toDoDOM, isFirstRender);
    renderList(doneListData, doneDOM, isFirstRender);
    // cleanup
    listData = listData.filter((item) => item.task !== TASK_DELETE);
    listData = listData.map((item) => {
      item.task = TASK_NONE;
      return item;
    });
    // Finally, save the data
    setLocalStorage(listData);
  };
  window.requestAnimationFrame(callback);
}
```

The function takes one argument; `isFirstRender`. This is only ever true on the first call. It prevents any animations being added to tickets on initial render.

The function sets up and runs a `requestAnimationFrame` callback, so it runs just before the next screen redraw. This ensures if we end up with multiple render calls, they will be naturally batched to just before the next available screen redraw.

Within the function, `listData` is split into two lists via `Array.filter` to give us `toDoListData` and `doneListData`. These two temporary lists are re-created every render.

We then render into our two DOM lists via the two `renderList()` calls.

> Note that our two data lists will both contain the same ticket if the operation is a `TASK_MOVE`. A move requires a deletion in the 'from' list and an addition to the 'to' list, which represents a task per list. When the data is rendered to the DOM, you only see the 'to' ticket.

### renderList()

> **Top level description**: The `renderList` function converts tasks into DOM changes.

```javascript
function renderList(theList, theDomList, isFirstRender) {
  const parentId = `#${theDomList.id}`;
  let prevQS = null;

  theList.forEach((item) => {
    const task = item.task;
    const qs = `${parentId} #${item.id}`;
    let newTicket;
    let foundTicket;

    switch (task) {
      case TASK_NONE:
        // Do nothing.
        break;

      case TASK_NEW:
        newTicket = renderTicket(item);
        if (!isFirstRender) {
          newTicket.classList.add("addItem");
        }
        if (!prevQS) {
          theDomList.prepend(newTicket);
        } else {
          foundTicket = document.querySelector(prevQS);
          foundTicket.after(newTicket);
        }
        break;

      case TASK_MOVE:
        foundTicket = document.querySelector(qs);
        if (foundTicket) {
          // The ticket already exists so is the 'from' in a 
          // move. We should delete it. NB - we don't use the
          // transitionend event because it is not reliable.
          foundTicket.classList.add("removeItem");
          window.setTimeout(() => {
            foundTicket.remove();
          }, 150);
        } else if (!prevQS) {
          newTicket = renderTicket(item);
          newTicket.classList.add("addItem");
          theDomList.prepend(newTicket);
        } else {
          newTicket = renderTicket(item);
          newTicket.classList.add("addItem");
          foundTicket = document.querySelector(prevQS);
          foundTicket.after(newTicket);
        }
        break;

      case TASK_DELETE:
        foundTicket = document.querySelector(qs);
        foundTicket.classList.add("removeItem");
        window.setTimeout(() => {
          foundTicket.remove();
        }, 150);
        break;

      case TASK_CHANGE:
        foundTicket = document.querySelector(qs);
        newTicket = renderTicket(item);
        newTicket.classList.add("changeFlash");
        foundTicket.replaceWith(newTicket);
        break;

      case TASK_RENUMBER:
        foundTicket = document.querySelector(qs);
        foundTicket.getElementsByClassName("itemNumber")[0].innerText =
          item.ticketNumber;
        break;

      default:
      // do nothing.
    }
    if (task !== TASK_DELETE) {
      prevQS = qs;
    }
  });
}
```

This function runs twice: once for each list. It is actually very simple; for each task, it finds the DOM to change via a class query-selector based on the data `id`, then does something to this bit of DOM depending on the task itself. Easy!

If it doesn't look easy, take a look at the following JavaScript language definitions:

[document.getElementById()](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)

[document.querySelector()](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector)

[ParentNode.prepend()](https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/prepend)

[ChildNode.after()](https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/after)

[ChildNode.remove()](https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/remove)

[HTML DOM classList](https://www.w3schools.com/jsref/prop_element_classlist.asp)

This list contains many of the most used parts of the [JavaScript Document Object Model](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model).

> A point to notice is why/when the code uses `document.getElementById()` and when it uses `document.querySelector()`. You use the first to find a particular node, searching the entire document on `id`. You use `document.querySelector()` when you are looking for one or more nodes _within a particular part of the DOM_, and use css classes to narrow the search. This all means you can only use id to find a node in the entire document. If you need to find a node from a starting point other than the entire document, you _have_ to search on class name.

As well as doing the main DOM manipulation this function also adds the animation classes `addItem`, `removeItem` and `changeFlash`.

> Note that when deleting a ticket, we wait 150ms before actually deleting it. This is to allow the user to see the `removeItem` animation.

The only call not covered in this function is `renderTicket()`. This is called when we want to draw or redraw (change) a ticket.

### renderTicket()

> **Top level description**: The `renderTicket` function takes data for a single ticket and returns the corresponding DOM for that one ticket.

```javascript
function renderTicket(item) {
  // Renders a single ticket
  // Copies the ticket template, fills in the ticket data and adds
  // interaction handlers. Note - the ticket is cloned from
  // the 'library' div in index.html. This saves having to write
  // JS code to create the ticket from scratch!
  const ticket = document.getElementById("library_listItem");
  const clone = ticket.cloneNode(true);
  const itemMover = clone.getElementsByClassName("itemMover")[0];
  const moveUp = itemMover.getElementsByClassName("moveUp")[0];
  const moveDown = itemMover.getElementsByClassName("moveDown")[0];
  const moveLeftIcon = clone.getElementsByClassName("moveLeft")[0];
  const moveRightIcon = clone.getElementsByClassName("moveRight")[0];
  const deleteIcon = clone.getElementsByClassName("delete")[0];
  const itemContent = clone.getElementsByClassName("itemContent")[0];
  const itemNumber = clone.getElementsByClassName("itemNumber")[0];
  const numberContainer = clone.getElementsByClassName("numberContainer")[0];

  // Add data.
  clone.id = item.id;
  itemNumber.innerText = item.ticketNumber || "--";
  itemContent.innerText = item.msg;

  // Add interaction handlers and show/hide contextual ui.
  itemMover.classList.add("hidden");
  moveUp.addEventListener("click", () => onMoveUp(item.id));
  moveDown.addEventListener("click", () => onMoveDown(item.id));
  itemContent.addEventListener("focus", onTextFocus);
  itemContent.addEventListener("blur", (e) =>
    onTextChange(item.id, item.isDefaultNew, e)
  );
  if (item.isDefaultNew) {
    // New ticket. The text has the 'new' class. No icons shown.
    itemContent.classList.add("new");
    moveLeftIcon.classList.add("hidden");
    moveRightIcon.classList.add("hidden");
    deleteIcon.classList.add("hidden");
  } else if (item.msg === "" || item.msg === "\n") {
    // Ticket with no content. Can be deleted but not moved.
    deleteIcon.addEventListener("click", () => onDelete(item.id));
    moveLeftIcon.classList.add("hidden");
    moveRightIcon.classList.add("hidden");
  } else if (!item.done) {
    // A 'todo' ticket with content. Can be moved right to 'done',
    // and may be moved up or down (as long as it is not the first
    // or last item in the list).
    numberContainer.addEventListener("mouseenter", () => {
      const isFirst = isFirstDone(item);
      const isLast = isLastDone(item);
      if(!(isFirst && isLast)) {
        itemNumber.classList.add("hidden");
        itemMover.classList.remove("hidden");
        if (!isFirst) {
          moveUp.classList.remove("disabled");
        } else {
          moveUp.classList.add("disabled");
        }
        if (!isLast) {
          moveDown.classList.remove("disabled");
        } else {
          moveDown.classList.add("disabled");
        }
      }
    });
    numberContainer.addEventListener("mouseleave", () => {
      itemNumber.classList.remove("hidden");
      itemMover.classList.add("hidden");
    });
    moveLeftIcon.classList.add("hidden");
    moveRightIcon.addEventListener("click", () => onMoveRight(item.id));
    deleteIcon.classList.add("hidden");
  } else {
    // A 'done' ticket with content. Can be moved left to 'todo' or deleted.
    moveLeftIcon.addEventListener("click", () => onMoveLeft(item.id));
    moveRightIcon.classList.add("hidden");
    deleteIcon.addEventListener("click", () => onDelete(item.id));
  }
  return clone;
}
```

Looks a little scary, but it is actually the simplest function so far... there's just a lot of it! Here's what it does:

* First, it clones the ticket markup in the hidden library container with id `library_listItem'.
* It then gets references to all the bits of this clone we want to change or attach interactivity to.
* Finally, it adds dynamic styling (based on current data) and event handlers to the references in the previous step.

> Note that there are only three variations of ticket; the default-new one, a ticket that has no message but is not the default, and a ticket with a message. The `renderTicket()` function does not know (or care) whether a ticket is in the to-do or done lists. That is handled via CSS.

The renderTicket() function calls a number of functions as event handlers. Most of them just call `onChange()` (the only ones that don't do something so trivial that they can go straight to `render()`).

### onChange()

> **Top level description**: The `onChange` function manages any knock-on data changes required on user interaction, and once the new data is set, calls a re-render.

```javascript
function onChange(id, newData) {
  // Handles a request to change the data. Once the data has changed,
  // we can re-render the view.
  listData.some((item, index, array) => {
    const isFound = item.id === id;
    if (isFound) {
      array[index] = { ...item, ...newData };
      // If the default-new ticket now no longer exists, create a new one
      // and reNumber all tickets. Note that this is a subtle point to know
      // before the logic makes sense - the code adds a new ticket by;
      // 1. Promoting the new ticket to a 'full ticket' with isDefaultNew:false.
      // 2. Creating a 'default new ticket' (with isDefaultNew:true) at the top
      //    of the todo (left) list.
      if (!listData[0].isDefaultNew) {
        listData.unshift(makeNewTicketData());
        reNumberTickets();
      }
      render();
    }
    return isFound;
  });
}
```

For some trivial data changes (such as moving a ticket up or down, where all we do is cheat by swapping the messages around!), we don't hit this function. For all other changes, we have to insert the changed data into the `listData` array, renumber any  tickets that have changed position, and then render based on the new list. The function also creates a new default ticket if the last operation made the previous one into a full ticket.

And that is the full application!

[_back to top_](#markdown-header-contents)

## FAQ

### Current features

Q: _Why can't you..._

| PotentiallyMissingFeature | Reason for omission |
|---|---|
| ...reorder tickets in the **Done** column? | Because a done ticket is considered 'spent' so the order should not matter. You can still reorder a ticket's position by moving back to the **To-do** list and moving it up/down there, then moving it back to **Done**.
| ...delete a ticket in the **To do** column? | Edit the ticket message to blank and the delete icon will appear. A ticket with no message is considered 'spent' because it has no value, and can be deleted.|
| ...clear the to-do list? | With the To-do list open in the browser, open dev tools (press ***F12***). In Chrome and MS Edge, select the ***Application Tab***. In the sidebar, select Storage > LocalStorage > the url of the To-do (if you are running locally, it wills tart with `file://`). In the table to right, click the line including `jsOnlyToDoList` and press ***Delete*** on your keyboard. In Firefox it is very similar except you look in the ***Storage tab***.|

### Bugs

Q: Why does...

| PotentiallyBuggedFeature | Reason for omission |
|---|---|
| My To-do list occasionally disappear? | Each different browser will save a different to-do list. So if you have both Chrome and Firefox installed, each will give you a different list. Stick to the same browser and all will be well, or use the different browsers to give you an office and home list! |

### Missing features

Q: _Why didn't you add...'_.

| NewRequiredFeature | Reason for omission |
|---|---|
| ...full cross browser support? | I have tested cross-browser support only for those combinations available to me. These were the latest versions of Chrome, Firefox and Edge on a Windows 10 machine, and Chrome and Firefox on Android. There may be compatibility problems on other operating systems. |
| ...drag-drop? | The user requirements were written assuming no drag-drop, so adding drag-drop now will invalidate most design assumptions. More to the point, drag-drop is not needed. Have a look at [this repo](https://bitbucket.org/shamb/kanban2/src/master/) for a ReactJS project that has drag/drop and animations, plus a search filter. |
| ...ability to export a to-do list so I can use it on different machines? | See Appendix A for a currently non-implemented stretch-goal requirement to do this. |

[_back to top_](#markdown-header-contents)

## Appendices

### Appendix A: User requirements

#### Initial view

![Requirements: initial view](/readme-resources/requirements-01.png)

The initial view will show two lists, marked To do and Done. An empty ticket will appear at the top of the To do list.

#### Creating a new ticket

![Requirements: create ticket](/readme-resources/requirements-02.png)

A new ticket can be created by adding a message to the default-new ticket. As soon as the ticket has a non-zero message entered, it becomes a full ticket in the To do column. It will be given a ticket number and a new empty ticket created above it.

#### Editing ticket messages

Any ticket can be edited by clicking on the message text and typing in new text. All common OS keyboard text editing short-cuts (copy, paste, etc) should be available.

#### Moving tickets

![Requirements: move ticket](/readme-resources/requirements-03.png)

A ticket can be moved to the adjacent list by clicking a move-left or move-right icon on the ticket. Only one of the two will appear, depending on the available direction.

A ticket can be moved up and down within the same list via a move up and move down icon.

#### Deleting tickets

![Requirements: delete ticket](/readme-resources/requirements-04.png)

A ticket in the To do column can be deleted if it has an empty message. A done ticket can be deleted if it is in the done list. In both cases, a delete icon will appear on the ticket to enable deletion.

#### Responsive layout

If the user is on a mobile or other narrow portrait display, the two columns will become two rows as shown.

![Requirements: Responsive design](/readme-resources/requirements-05.png)

#### System tickets

TODO: this requirement has not yet been implemented.

The user can enter three specific ticket messages when creating a new ticket that are system commands. These system commands manage the To do list itself. They are listed below.

| command | Description |
|---|---|
| #export | Exports the current list data as a text file. The file will be called todo_YYYY_MM_DD where YYYY_MM_DD are the current date. Such a ticket will be moved to the Done list when export completes with the message 'Exported_xxxx` (where xxxx is the filename) if export succeeds, or with 'Error: export failed' otherwise.
| #import | Causes the application to load a previously exported list data. If export succeeds, the ticket will move to Done with the message 'Import complete', or 'Import failed' otherwise.
| #clearListNow | The application will be reset and all current data cleared.
