(function main() {
  function render(isFirstRender = false) {
    // Renders the application.
    // Takes the single toDo list data, splitting it into 'todo' and
    // 'done' lists, and then render into the two lists.
    // Note that a move has to be in both lists (because is is a
    // 'delete' in one list and an 'add' in the other).
    const callback = () => {
      const toDoListData = listData.filter(
        (item) => !item.done || item.task === TASK_MOVE
      );
      const doneListData = listData.filter(
        (item) => item.done || item.task === TASK_MOVE
      );
      renderList(toDoListData, toDoDOM, isFirstRender);
      renderList(doneListData, doneDOM, isFirstRender);
      // cleanup
      listData = listData.filter((item) => item.task !== TASK_DELETE);
      listData = listData.map((item) => {
        item.task = TASK_NONE;
        return item;
      });
      // Finally, save the data
      setLocalStorage(listData);
    };
    window.requestAnimationFrame(callback);
  }

  function renderList(theList, theDomList, isFirstRender) {
    const parentId = `#${theDomList.id}`;
    let prevQS = null;

    theList.forEach((item) => {
      const task = item.task;
      const qs = `${parentId} #${item.id}`;
      let newTicket;
      let foundTicket;

      switch (task) {
        case TASK_NONE:
          // Do nothing.
          break;

        case TASK_NEW:
          newTicket = renderTicket(item);
          if (!isFirstRender) {
            newTicket.classList.add("addItem");
          }
          if (!prevQS) {
            theDomList.prepend(newTicket);
          } else {
            foundTicket = document.querySelector(prevQS);
            foundTicket.after(newTicket);
          }
          break;

        case TASK_MOVE:
          foundTicket = document.querySelector(qs);
          if (foundTicket) {
            // The ticket already exists so is the 'from' in a
            // move. We should delete it. NB - we don't use the
            // transitionend event because it is not reliable.
            foundTicket.classList.add("removeItem");
            window.setTimeout(() => {
              foundTicket.remove();
            }, 200);
          } else if (!prevQS) {
            newTicket = renderTicket(item);
            newTicket.classList.add("addItem");
            theDomList.prepend(newTicket);
          } else {
            newTicket = renderTicket(item);
            newTicket.classList.add("addItem");
            foundTicket = document.querySelector(prevQS);
            foundTicket.after(newTicket);
          }
          break;

        case TASK_DELETE:
          foundTicket = document.querySelector(qs);
          foundTicket.classList.add("removeItem");
          window.setTimeout(() => {
            foundTicket.remove();
          }, 200);
          break;

        case TASK_CHANGE:
          foundTicket = document.querySelector(qs);
          newTicket = renderTicket(item);
          newTicket.classList.add("changeFlash");
          foundTicket.replaceWith(newTicket);
          break;

        case TASK_RENUMBER:
          foundTicket = document.querySelector(qs);
          foundTicket.getElementsByClassName("itemNumber")[0].innerText =
            item.ticketNumber;
          break;

        default:
        // do nothing.
      }
      if (task !== TASK_DELETE) {
        prevQS = qs;
      }
    });
  }

  function renderTicket(item) {
    // Renders a single ticket
    // Copies the ticket template, fills in the ticket data and adds
    // interaction handlers. Note - the ticket is cloned from
    // the 'library' div in index.html. This saves having to write
    // JS code to create the ticket from scratch!
    const ticket = document.getElementById("library_listItem");
    const clone = ticket.cloneNode(true);
    const itemMover = clone.getElementsByClassName("itemMover")[0];
    const moveUp = itemMover.getElementsByClassName("moveUp")[0];
    const moveDown = itemMover.getElementsByClassName("moveDown")[0];
    const moveLeftIcon = clone.getElementsByClassName("moveLeft")[0];
    const moveRightIcon = clone.getElementsByClassName("moveRight")[0];
    const deleteIcon = clone.getElementsByClassName("delete")[0];
    const itemContent = clone.getElementsByClassName("itemContent")[0];
    const itemNumber = clone.getElementsByClassName("itemNumber")[0];
    const numberContainer = clone.getElementsByClassName("numberContainer")[0];

    // Add data.
    clone.id = item.id;
    itemNumber.innerText = item.ticketNumber || "--";
    itemContent.innerText = item.msg;

    // Add interaction handlers and show/hide contextual ui.
    itemMover.classList.add("hidden");
    moveUp.addEventListener("click", () => onMoveUp(item.id));
    moveDown.addEventListener("click", () => onMoveDown(item.id));
    itemContent.addEventListener("focus", onTextFocus);
    itemContent.addEventListener("blur", (e) =>
      onTextChange(item.id, item.isDefaultNew, e)
    );
    if (item.isDefaultNew) {
      // New ticket. The text has the 'new' class. No icons shown.
      itemContent.classList.add("new");
      moveLeftIcon.classList.add("hidden");
      moveRightIcon.classList.add("hidden");
      deleteIcon.classList.add("hidden");
    } else if (item.msg === "" || item.msg === "\n") {
      // Ticket with no content. Can be deleted but not moved.
      deleteIcon.addEventListener("click", () => onDelete(item.id));
      moveLeftIcon.classList.add("hidden");
      moveRightIcon.classList.add("hidden");
    } else if (!item.done) {
      // A 'todo' ticket with content. Can be moved right to 'done',
      // and may be moved up or down (as long as it is not the first
      // or last item in the list).
      numberContainer.addEventListener("mouseenter", () => {
        const isFirst = isFirstDone(item);
        const isLast = isLastDone(item);
        if (!(isFirst && isLast)) {
          itemNumber.classList.add("hidden");
          itemMover.classList.remove("hidden");
          if (!isFirst) {
            moveUp.classList.remove("disabled");
          } else {
            moveUp.classList.add("disabled");
          }
          if (!isLast) {
            moveDown.classList.remove("disabled");
          } else {
            moveDown.classList.add("disabled");
          }
        }
      });
      numberContainer.addEventListener("mouseleave", () => {
        itemNumber.classList.remove("hidden");
        itemMover.classList.add("hidden");
      });
      moveLeftIcon.classList.add("hidden");
      moveRightIcon.addEventListener("click", () => onMoveRight(item.id));
      deleteIcon.classList.add("hidden");
    } else {
      // A 'done' ticket with content. Can be moved left to 'todo' or deleted.
      moveLeftIcon.addEventListener("click", () => onMoveLeft(item.id));
      moveRightIcon.classList.add("hidden");
      deleteIcon.addEventListener("click", () => onDelete(item.id));
    }
    return clone;
  }

  function isFirstDone(item) {
    const findFirstNotDone = (item, index) => index !== 0 && !item.done;
    const firstNotDonePos = listData.findIndex(findFirstNotDone);
    // Returns true if the first done:false ticket is item but not
    // the default-new ticket. Used to hide the moveUp button when
    // the selected ticket is the top one.
    return item.ticketNumber === firstNotDonePos;
  }

  function isLastDone(item) {
    let lastNotDonePos = -1;
    // Returns true if the last done;false ticket is item.
    // Used to hide the moveDown button when the selected
    // ticket is the bottom one.
    for (let i = listData.length - 1; i > -1; i--) {
      if (listData[i].done === false) {
        lastNotDonePos = i;
        break;
      }
    }
    return item.ticketNumber === lastNotDonePos;
  }

  function onTextFocus(e) {
    // The event handler for text entry start. Note that we are
    // using  contenteditable rather than an input. The latter
    // allows us to add proper text formatting (bold, italic, etc)
    // in real time later, which is a big win of this method.
    if (e.target.innerText === NEW_TO_DO) {
      e.target.innerText = "";
    }
    e.target.classList.remove("new");
  }

  function onTextChange(id, isDefaultNew, e) {
    // Event handler for text entry end.
    console.log("textChange ", id);
    const newText = e.target.innerText;
    if (newText !== "" && newText !== "\n" && isDefaultNew) {
      // The default-new ticket now has content, is no longer new,
      // and can have a ticketNumber.
      onChange(id, {
        msg: newText,
        isDefaultNew: false,
        ticketNumber: 1,
        task: TASK_CHANGE,
      });
    } else if ((newText === "" || newText === "\n") && isDefaultNew) {
      // The default-new ticket has had "" entered. Revert it to
      // the default view onscreen (no data change).
      e.target.classList.add("new");
      e.target.innerText = NEW_TO_DO;
    } else if (!isDefaultNew) {
      // An existing ticket has changed. kick a redraw
      onChange(id, { msg: newText, isDefaultNew: false, task: TASK_CHANGE });
    }
  }

  function onMoveUp(id) {
    // Event handler for the move up list-item icon.
    console.log("move up ", id);
    let to, from;
    listData.some((item) => {
      const isFound = item.id === id;
      if (isFound) {
        from = item;
      } else if (!item.done) {
        to = item;
      }
      return isFound;
    });
    swapMessage(from, to);
    render();
  }

  function onMoveDown(id) {
    // Event handler for the move down list-item icon.
    console.log("move down ", id);
    let to = null;
    let from = null;
    let hasFoundFrom = false;
    listData.some((item) => {
      if (item.id === id) {
        from = item;
        hasFoundFrom = true;
      } else if (hasFoundFrom && !item.done) {
        to = item;
      }
      return from && to;
    });
    swapMessage(from, to);
    render();
  }

  function swapMessage(from, to) {
    // Swaps the message between two tickets.
    from.task = TASK_CHANGE;
    fromMsg = from.msg;
    from.msg = to.msg;
    to.msg = fromMsg;
    to.task = TASK_CHANGE;
  }

  function onMoveLeft(id) {
    // Event handler for the <- list-item icon.
    console.log("moveLeft ", id);
    onChange(id, { done: false, task: TASK_MOVE });
  }

  function onMoveRight(id) {
    // Event handler for the -> list-item icon.
    console.log("moveRight ", id);
    onChange(id, { done: true, task: TASK_MOVE });
  }

  function onDelete(id) {
    // Event handler for the x list-item icon.
    console.log("delete", id);
    listData.some((item, index) => {
      const isFound = item.id === id;
      if (isFound) {
        listData[index].task = TASK_DELETE;
      }
      return isFound;
    });
    reNumberTickets();
    render();
  }

  function onChange(id, newData) {
    // Handles a request to change the data. Once the data has changed,
    // we can re-render the view.
    listData.some((item, index, array) => {
      const isFound = item.id === id;
      if (isFound) {
        array[index] = { ...item, ...newData };
        // If the default-new ticket now no longer exists, create a new one
        // and reNumber all tickets. Note that this is a subtle point to know
        // before the logic makes sense - the code adds a new ticket by;
        // 1. Promoting the new ticket to a 'full ticket' with isDefaultNew:false.
        // 2. Creating a 'default new ticket' (with isDefaultNew:true) at the top
        //    of the todo (left) list.
        if (!listData[0].isDefaultNew) {
          listData.unshift(makeNewTicketData());
          reNumberTickets();
        }
        render();
      }
      return isFound;
    });
  }

  function reNumberTickets() {
    let ticketNumber = 1;
    listData = listData.map((item) => {
      if (!item.isDefaultNew) {
        if (item.task !== TASK_DELETE) {
          item.ticketNumber = ticketNumber++;
        }
        if (item.task === TASK_NONE) {
          item.task = TASK_RENUMBER;
        }
      }
      return item;
    });
  }

  function makeNewTicketData() {
    // Return a default new ticket.
    const MIN_RANGE = 200000000000000;
    const MAX_RANGE = Number.MAX_SAFE_INTEGER;
    // id gives a unique string id, e.g. 'ticket_23g6xawvgw2', where the random string
    // is at least 10 characters long.
    let rnd = Math.floor(Math.random() * MAX_RANGE);
    if (rnd < MIN_RANGE) {
      rnd = rnd + MIN_RANGE;
    }
    const id = `ticket_${rnd.toString(36)}`;

    return {
      msg: NEW_TO_DO,
      isDefaultNew: true,
      done: false,
      id: id,
      ticketNumber: null,
      task: TASK_NEW,
    };
  }

  function getLocalStorage(localStorage) {
    let savedListData = localStorage.getItem("jsOnlyToDoList");
    if (savedListData) {
      savedListData = JSON.parse(savedListData);
      // As this will be a first render, force all data to be
      // redrawn by making it 'new'.
      savedListData = savedListData.map((item) => {
        item.task = TASK_NEW;
        return item;
      });
    } else {
      savedListData = [makeNewTicketData()];
      localStorage.setItem("jsOnlyToDoList", JSON.stringify(savedListData));
    }
    return savedListData;
  }

  function setLocalStorage(data) {
    localStorage.setItem("jsOnlyToDoList", JSON.stringify(data));
  }

  function stopAttractor() {
    // Remove the click handler on container, then remove the 
    // attractor div.
    document
      .getElementById("container")
      .removeEventListener("click", stopAttractor);
    document.getElementById("attractor").remove();
  }

  /**
   * Initialisation.
   * 1. Create the initial data (consisting of one default ticket) and any constants or the last data seen in localStorage.
   * 2. Find the two lists (todo and done).
   * 3. Do the initial render.
   * 4. Add interactivity for the 'attractor' message if necessary, otherwise delete the attractor.
   *
   * The application will then continue to run based on user interaction
   * i.e. adding/moving/deleting tickets.
   *  */

  const NEW_TO_DO = "New to-do";
  const TASK_NONE = "none";
  const TASK_NEW = "actionNew";
  const TASK_MOVE = "actionMove";
  const TASK_DELETE = "actionDelete";
  const TASK_CHANGE = "actionChange";
  const TASK_RENUMBER = "actionRenumber";
  // 1
  let localStorage = window.localStorage;
  let listData = getLocalStorage(localStorage);
  // 2
  const toDoDOM = document.getElementById("toDo");
  const doneDOM = document.getElementById("done");
  // 3
  render(true);
  // 4
  if (listData.length > 1) {
    document.getElementById("attractor").remove();
  } else {
    document.getElementById("container").addEventListener("click", stopAttractor);
  }
})();
